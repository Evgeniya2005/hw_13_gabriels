<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

require_once 'config/db.php';

try{
	$sql = 'SELECT * FROM good';
	$result = $db->query($sql);
	$goods = $result->fetchAll();

}catch(Exception $e){
	$message = 'Error getting data'.$e->getMessage();
	die($message);
}
?>


<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title> Document </title>
</head>
<body>
	<ul>
		<li>
			<a href="create_db.php">Create DB tables</a>
		</li>
		<li>
			<a href="createGood.php">Add Joke</a>
		</li>	
	</ul>
	<div>
		<ul>
		<h1> GOODS: </h1>
  		
		<?php foreach ($goods as $good): ?>
			 <li> <?= $good['id']?> - <?= $good['title']?> 
			 <a href="showgood.php?goodId=<?=$good['id']?>"> SHOW </a>
			 <a href="editGood.php?goodId=<?= $good['id']?>">Edit</a> 
				<form action="deleteGood.php" method="POST">
					<input type="hidden" name='goodId' value="<?= $good['id']?>">
					<button> DELETE </button>
				</form></li>
		<?php endforeach; ?>
	</ul>
	</div>

	
</body>
</html>

