<?php
require_once 'config/db.php';

$goods=[
  [
  'title' => 'Samsung Galaxy S10',
  'price' => '24999',
  'description' => 'Иммерсивный 6,1” Quad HD+ экран Galaxy S10 1 обладает невероятно высокой контрастностью и улучшенной цветопередачей. Благодаря технологии высокоточной лазерной обработки камера скрыта внутри экрана, при этом сохранено высокое качество фотографии',
  'type' => 'Phone',
  ],
	[
  'title' => 'Apple MacBook 12',
  'price' => '27500',
  'description' => 'Создавая MacBook, мы поставили перед собой цель, которая казалась невозможной: добиться абсолютной функциональности и удобства самого тонкого и лёгкого ноутбука Mac. При своей компактности он мощнее предыдущих моделей. Производительность нового MacBook выросла на 20% — он оснащён новыми процессорами Intel Core i5 - седьмого поколения и SSD‑накопителями 256 и 512 Гб, которые увеличили скорость до 50%.',
  'type' => 'Laptop',
  ],
	[
  'title' => 'Apple Watch Series 5',
  'price' => '13499',
  'description' => 'Apple Watch Series 5',
  'type' => 'Watch',
  ],	
];

$sql ='INSERT INTO good SET
title=:title,
price =:price,
description =:description,
type =:type';


try{
  foreach ($goods as $good) {
    $goodObj=$db->prepare($sql);
    $goodObj->bindValue(':title',$good['title']);
    $goodObj->bindValue(':price',$good['price']);
    $goodObj->bindValue(':description',$good['description']);
    $goodObj->bindValue(':type',$good['type']);
    $goodObj->execute();
  }
}catch(Exception $e){
  $message='Database create test members error'.$e;
  die($message);
}

?>