<?php
require_once 'config/db.php';
require_once 'Classes/good.class.php';

	$goodId = $_GET['goodId'];
	$goodObj=Good::create($goodId,$db);	
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Document</title>
</head>
<body>
	<h1> Lets edit good :</h1>
	<form action="updateGood.php" method="POST">
		<fieldset>
			<label>
				Title Good: <input type="text" name='titlegood' value="<?=$goodObj->getTitle()?>">
				<br>
				Price Good: <input type="number" name='pricegood' value="<?=$goodObj->getPrice()?>">
				<br>
				Description Good: <input type="text" name='descriptiongood' value="<?=$goodObj->getDescription()?>">
				<br>
				Type Good: <input type="text" name='typegood' value="<?=$goodObj->getType()?>">
				<input type="hidden" name='goodId' value="<?=$goodObj->getId()?>">
				
			</label>
			<br>
			<button>Save Good!</button>
		</fieldset>
	</form>
</body>
</html>