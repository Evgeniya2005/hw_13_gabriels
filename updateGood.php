<?php
require_once 'config/db.php';
require_once 'Classes/good.class.php';

$title = $_POST['titlegood'];
$price = $_POST['pricegood'];
$description = $_POST['descriptiongood'];
$type = $_POST['typegood'];
$id =  $_POST['goodId'];

$goodObj = Good::create($id,  $db);
$goodObj->setTitle($title);
$goodObj->setPrice($price);
$goodObj->setType($type);
$goodObj->setDescription($description);
$goodObj->update($db);


header('Location:index.php');


