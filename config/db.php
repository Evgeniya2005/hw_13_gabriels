<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

try{
	$db = new PDO('mysql:host=localhost;dbname=goods','root','');
	$db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
	$db->exec('SET NAMES "utf8"');

}catch(Exception $error){
	$output = 'Technical problems, please come later '.$error->getMessage();
	die($output);
}
?>