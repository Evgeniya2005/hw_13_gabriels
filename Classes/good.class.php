<?php

class Good
{
	protected $title;
	protected $price;
	protected $description;
	protected $type;
	protected $id;


	
	function __construct($title,$price,$description,$type)
	{
		$this->title=$title;
		$this->price=$price;
		$this->description=$description;
		$this->type=$type;
	}
	
	static public function create ($id, PDO $db){
		try{
			$sql = 'SELECT * FROM good WHERE id =:id';
			$goodObj=$db->prepare($sql);
			$goodObj->bindValue(':id',$id);
			$goodObj->execute();
			$good=$goodObj->fetch();

		}catch(Exception $e){
			$message ='Error displying good: '. $e->getMessage();
			die($message );
		}
		$goodObj = new Good($good['title'],$good['price'],$good['description'],$good['type']);
		$goodObj->setId($good['id']);
		return $goodObj;
	}

	public function save ($pdo){
		try {
		$sql = "INSERT INTO good SET 
			title = :titlegood,
			price = :pricegood,
			description = :descriptiongood,
			type = :typegood
		";
		$storeObject = $pdo->prepare($sql);
		$storeObject->bindValue(':titlegood', $this->title);
		$storeObject->bindValue(':pricegood', $this->price);
		$storeObject->bindValue(':descriptiongood', $this->description);
		$storeObject->bindValue(':typegood', $this->type);
		$storeObject->execute();

		} catch (Exception $e) {
			$message = "Error creating good" . $e->getMessage();
			die($message);
		}
	}

	public function update (PDO $pdo){
		try {
			$sql = "UPDATE good SET 
			title = :titlegood,
			price = :pricegood,
			description = :descriptiongood,
			type = :typegood
			WHERE id = :id";	 
			$updateObj = $pdo->prepare($sql);
			$updateObj->bindValue(':titlegood',$this->title);
			$updateObj->bindValue(':pricegood',$this->price);
			$updateObj->bindValue(':descriptiongood',$this->description);
			$updateObj->bindValue(':typegood',$this->type);
			$updateObj->bindValue(':id', $this->id);
			$updateObj->execute();
		} catch (Exception $e) {
			$message = "Error updating good" . $e->getMessage();
			die($message);
		}
		}

	static function delete ($id, PDO $pdo){
				try {
			$sql = "DELETE FROM good WHERE id=:id";	 
			$deleteObj = $pdo->prepare($sql);
			$deleteObj->bindValue(':id', $id);
			$deleteObj->execute();
			} catch (Exception $e) {
				$message = "Error deleting good" . $e->getMessage();
				die($message);
			}
	}	
	public function getId(){
		return $this->id;
	}

	public function setId($id){
		$this->id=$id;
	}
	public function getTitle(){
		return $this->title;
	}
	
	public function getType(){
		return $this->type;
	}
	public function getPrice(){
		return $this->price;
	}
	public function getDescription(){
		return $this->description;
	}
	public function setTitle($title){
		$this->title=$title;
	}
	public function setPrice($price){
		$this->price=$price;
	}
	public function setType($type){
		$this->type=$type;
	}
	public function setDescription($description){
		$this->description=$description;
	}

}